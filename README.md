## Install
1. Tested on Ubuntu 16.04 LTS 64bit.
1. Application is tested on php 7.3.13. Run it from host 
   ```bash
   php -f html/public/index.php
   ```
   Explicitly use data file 
   ```bash
   php -f html/public/index.php html/var/storage/input.txt
   ```
   Or continue with docker.
1. Build and run application docker container
   ```bash
   docker build -t vinted-delivery  ./
   docker run -d --name vinted-delivery -v `pwd`/html:/var/www/html vinted-delivery
   ```
1. Run application with default source file
   ```bash
   docker exec -ti vinted-delivery php -f public/index.php 
   ```
   Or use custom data file
   ```bash
   docker exec -ti vinted-delivery php -f public/index.php var/storage/input.txt
   ```
1. Stop and start docker if needed
   ```bash
   docker stop vinted-delivery
   docker start vinted-delivery
   ```
1. Destroy the container and image after application is evaluated  
   ```bash
   docker rm vinted-delivery
   docker rmi vinted-delivery
   ```
   
## Unit tests
   Run tests from "html" directory   
   ```bash
   phpunit
   ```

   Run test from host
   ```bash
   docker exec -ti vinted-delivery phpunit
   ```

   If you need PhpStorm support of PhpUnit place phpunit.phar to project root.
    
## Some thoughts
1. I would love to have no composer (composer autoloader), framework components (dependency injection, configuration).
1. Used carrier models not DB entities for simplicity.
1. Code standard is mixture of php and ruby.  
