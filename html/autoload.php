<?php

spl_autoload_register(
  function($className) {
    $className = str_replace(
      "\\",
      DIRECTORY_SEPARATOR,
      $className
    );
    $path = __DIR__ . DIRECTORY_SEPARATOR . $className . '.php';
    if (file_exists($path)) {
      require_once $path;
    }
  }
);