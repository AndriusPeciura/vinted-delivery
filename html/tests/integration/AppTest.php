<?php

namespace tests\integration;

use PHPUnit\Framework\TestCase;
use src\App;
use src\service\format\CSVFormatter;
use src\service\parser\ArgumentParser;
use src\service\parser\LineParser;
use src\service\price\PriceCalculator;

class AppTest extends TestCase
{
  /** @dataProvider data_provider */
  public function test_run(string $expected, array $args, string $description)
  {
    $app = new App(
      new ArgumentParser(),
      new LineParser(),
      new CSVFormatter(),
      new PriceCalculator()
    );

    $this->assertSame($expected, $app->run($args), $description);
  }

  public function data_provider()
  {
    $root = dirname(__DIR__) . DIRECTORY_SEPARATOR;
    return [
      [
        file_get_contents($root . 'resources/output.txt'),
        [1 => $root . 'resources/input.txt'],
        'Test shipping prices; S, L and limit discounts; incorrect lines.',
      ],
    ];
  }
}