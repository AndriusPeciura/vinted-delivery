<?php

namespace tests\integration\service\price;

use DateTime;
use PHPUnit\Framework\TestCase;
use src\model\Package;
use src\service\price\PriceCalculator;

class PriceCalculatorTest extends TestCase
{
  public function test_calculate()
  {
    $calculator = new PriceCalculator();

    foreach ($this->data_provider() as $row) {
      list($expected, $package) = $row;
      list($price, $discount) = $expected;
      $delivery_price = $calculator->calculate($package);
      $this->assertSame(
        $package,
        $delivery_price->get_package(),
        'Price is linked to package'
      );
      $this->assertSame(
        $discount,
        $delivery_price->get_discount(),
        'Matches expected discount'
      );
      $this->assertSame(
        $price,
        $delivery_price->get_price(),
        'Matches expected price'
      );
    }
  }

  private function data_provider()
  {
    return [
      [[1.5, 0.5,], new Package(new DateTime('2015-02-01'), 'S', 'MR'),],
      [[1.5, 0.5,], new Package(new DateTime('2015-02-02'), 'S', 'MR'),],
      [[6.9, 0.0,], new Package(new DateTime('2015-02-03'), 'L', 'LP'),],
      [[1.5, 0.0,], new Package(new DateTime('2015-02-05'), 'S', 'LP'),],
      [[1.5, 0.5,], new Package(new DateTime('2015-02-06'), 'S', 'MR'),],
      [[6.9, 0.0,], new Package(new DateTime('2015-02-06'), 'L', 'LP'),],
      [[4.0, 0.0,], new Package(new DateTime('2015-02-07'), 'L', 'MR'),],
      [[3.0, 0.0,], new Package(new DateTime('2015-02-08'), 'M', 'MR'),],
      [[0.0, 6.9,], new Package(new DateTime('2015-02-09'), 'L', 'LP'),],
      [[6.9, 0.0,], new Package(new DateTime('2015-02-10'), 'L', 'LP'),],
      [[1.5, 0.5,], new Package(new DateTime('2015-02-10'), 'S', 'MR'),],
      [[1.5, 0.5,], new Package(new DateTime('2015-02-10'), 'S', 'MR'),],
      [[6.9, 0.0,], new Package(new DateTime('2015-02-11'), 'L', 'LP'),],
      [[3.0, 0.0,], new Package(new DateTime('2015-02-12'), 'M', 'MR'),],
      [[4.9, 0.0,], new Package(new DateTime('2015-02-13'), 'M', 'LP'),],
      [[1.5, 0.5,], new Package(new DateTime('2015-02-15'), 'S', 'MR'),],
      [[6.9, 0.0,], new Package(new DateTime('2015-02-17'), 'L', 'LP'),],
      [[1.9, 0.1,], new Package(new DateTime('2015-02-17'), 'S', 'MR'),],
      [[6.9, 0.0,], new Package(new DateTime('2015-02-24'), 'L', 'LP'),],
      [[1.5, 0.5,], new Package(new DateTime('2015-03-01'), 'S', 'MR'),],
    ];
  }
}