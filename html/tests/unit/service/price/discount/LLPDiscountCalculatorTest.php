<?php

namespace tests\unit\service\price\discount;

use PHPUnit\Framework\TestCase;
use src\model\Package;
use src\service\price\discount\LLPDiscountCalculator;
use \DateTime;

class LLPDiscountCalculatorTest extends TestCase
{
  public function test_calculate()
  {
    $calculator = new LLPDiscountCalculator();
    foreach($this->data_provider() as $row){
      list($expected, $price, $package,$description) = $row;
      $this->assertSame(
        $expected,
        $calculator->calculate($price, $package),
        $description
      );
    }
  }

  /**
   * @return array[] [[$expected, $price, $package,$description], ...]
   */
  public function data_provider()
  {
    return [
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-01'), 'S', 'LP'),
        'No discount for S package by LP',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-02'), 'M', 'LP'),
        'No discount for M package by LP',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-03'), 'L', 'LP'),
        'No discount for L package by LP',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-01'), 'S', 'MR'),
        'No discount for S package by MR',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-02'), 'M', 'MR'),
        'No discount for M package by MR',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-03'), 'L', 'MR'),
        'No discount for L package by MR',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-05'), 'L', 'LP'),
        'No discount for L package by LP',
      ],
      [
        5.0,
        5.0,
        new Package(new DateTime('2000-02-06'), 'L', 'LP'),
        'Free shipping for 3rd L package by LP during one month',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-07'), 'L', 'LP'),
        'No discount for L package by LP',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-08'), 'L', 'LP'),
        'No discount for L package by LP',
      ],
      [
        0.0,
        5.0,
        new Package(new DateTime('2000-02-09'), 'L', 'LP'),
        'No discount for L package by LP',
      ],
    ];
  }
}