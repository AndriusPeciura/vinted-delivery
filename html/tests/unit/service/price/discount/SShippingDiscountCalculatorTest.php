<?php

namespace tests\unit\service\price\discount;

use PHPUnit\Framework\TestCase;
use src\model\carrier\CarrierBase;
use src\model\carrier\CarrierHash;
use src\model\Package;
use \DateTime;
use src\service\price\discount\SShippingDiscountCalculator;
use \ReflectionClass;

class SShippingDiscountCalculatorTest extends TestCase
{
  /** @dataProvider data_provider */
  public function test_calculate(
    float $expected,
    array $carrier_prices,
    float $price,
    Package $package,
    string $description
  ) {

    $this->assertSame(
      $expected,
      $this->build_calculator(...$carrier_prices)->calculate($price, $package),
      $description
    );
  }

  public function data_provider()
  {
    return [
      [
        0.0,
        [15, 20, 10, 30],
        22,
        new Package(new DateTime('2020-01-01'), 'M', 'LR'),
        'No discount for M size packages',
      ],
      [
        0.0,
        [15, 20, 10, 30],
        22,
        new Package(new DateTime('2020-01-01'), 'L', 'LR'),
        'No discount for L size packages',
      ],
      [
        12.0,
        [15, 20, 10, 30],
        22,
        new Package(new DateTime('2020-01-01'), 'S', 'LR'),
        'Discount 12 for S size packages',
      ],
      [
        19.0,
        [15, 20, 3, 30],
        22,
        new Package(new DateTime('2020-01-01'), 'S', 'MR'),
        'Discount 19 for S size packages',
      ],
      [
        0.0,
        [15, 20, 3, 30],
        3,
        new Package(new DateTime('2020-01-01'), 'S', 'MR'),
        'No discount for lowest price S size packages',
      ],
      [
        0.0,
        [15, 20, 3, 30],
        2,
        new Package(new DateTime('2020-01-01'), 'S', 'MR'),
        'No discount for price lower then lowest price',
      ],
    ];
  }

  private function build_calculator(...$carrier_prices)
  {
    $carrier = $this->getMockForAbstractClass(
      CarrierBase::class,
      [],
      '',
      true,
      true,
      true,
      ['get_price']
    );

    $carrier->expects($this->any())
      ->method('get_price')
      ->with('S')
      ->willReturnOnConsecutiveCalls( ...$carrier_prices);

    $carrier_hash = $this->getMockBuilder(CarrierHash::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['get_hash'])
      ->getMock();
    $carrier_hash->expects($this->any())
      ->method('get_hash')
      ->willReturn([$carrier, $carrier, $carrier, $carrier]);

    $calculator = new SShippingDiscountCalculator();
    $class = new ReflectionClass(SShippingDiscountCalculator::class);
    $property = $class->getProperty("carrier_hash");
    $property->setAccessible(true);
    $property->setValue($calculator, $carrier_hash);

    return $calculator;
  }
}