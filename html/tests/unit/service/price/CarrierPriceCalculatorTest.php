<?php

namespace tests\unit\service\price;

use DateTime;
use PHPUnit\Framework\TestCase;
use src\exception\InvalidArgumentException;
use src\model\carrier\CarrierBase;
use src\model\carrier\CarrierHash;
use src\model\Package;
use src\service\price\CarrierPriceCalculator;

class CarrierPriceCalculatorTest extends TestCase
{
  /** @dataProvider data_provider */
  public function test_calculate(
    float $expected,
    Package $package,
    string $description
  ) {
    $deliveryPrice = $this->build_calculator()->calculate($package);

    $this->assertSame(
      $package,
      $deliveryPrice->get_package(),
      'Delivery price is linked to package'
    );

    $this->assertSame(
      $expected,
      $deliveryPrice->get_price(),
      $description
    );
    $this->assertSame(
      null,
      $deliveryPrice->get_discount(),
      'Discount is not set'
    );
  }

  public function test_exception()
  {
    $this->expectException(InvalidArgumentException::class);

    (new CarrierPriceCalculator())->calculate(
      new Package(new DateTime('2020-01-01'), 'S', 'fake_carrier')
    );
  }

  public function data_provider()
  {
    return [
      [
        2.0,
        new Package(new DateTime('2020-01-01'), 'S', 'A'),
        'Carrier A size S price 2.0',
      ],
      [
        9.0,
        new Package(new DateTime('2020-01-01'), 'M', 'C'),
        'Carrier C size M price 9.0',
      ],
      [
        10.5,
        new Package(new DateTime('2020-01-01'), 'L', 'B'),
        'Carrier B size L price 10.5',
      ],
    ];
  }

  private function build_calculator()
  {
    $calculator = new CarrierPriceCalculator();
    $class = new \ReflectionClass(CarrierPriceCalculator::class);
    $property = $class->getProperty("carrier_hash");
    $property->setAccessible(true);
    $property->setValue($calculator, $this->build_carrier_hash());

    return $calculator;
  }

  private function build_carrier_hash()
  {
    $carrierA = $this->build_carrier([
      ['S', 2.0],
      ['M', 6.0],
      ['L', 20.0],
    ]);
    $carrierB = $this->build_carrier([
      ['S', 3.5],
      ['M', 7.0],
      ['L', 10.5],
    ]);
    $carrierC = $this->build_carrier([
      ['S', 5.0],
      ['M', 9.0],
      ['L', 13.0],
    ]);

    $carrier_hash = $this->getMockBuilder(CarrierHash::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['get'])
      ->getMock();
    $carrier_hash->expects($this->once())
      ->method('get')
      ->willReturnMap([
        ['A', $carrierA,],
        ['B', $carrierB,],
        ['C', $carrierC,],
      ]);

    return $carrier_hash;
  }

  private function build_carrier(array $return_map)
  {
    $carrier = $this->getMockForAbstractClass(
      CarrierBase::class,
      [],
      '',
      true,
      true,
      true,
      ['get_price']
    );

    $carrier->expects($this->any())
      ->method('get_price')
      ->willReturnMap($return_map);

    return $carrier;
  }
}