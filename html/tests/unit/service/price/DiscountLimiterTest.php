<?php

namespace tests\unit\service\price;

use PHPUnit\Framework\TestCase;
use src\model\Package;
use src\service\price\DiscountLimiter;

class DiscountLimiterTest extends TestCase
{
  public function test_limit()
  {
    $limiter = new DiscountLimiter();
    foreach ($this->data_provider() as $row) {
      list($expected, $discount, $package, $description) = $row;
      $this->assertSame(
        $expected,
        $limiter->limit($discount, $package),
        $description
      );
    }
  }

  private function data_provider()
  {
    return [
      [
        3.0,
        3,
        new Package(new \DateTime('2000-01-01'), 'L', 'X'),
        'Discount is not limited and equals 3',
      ],
      [
        0.0,
        0.0,
        new Package(new \DateTime('2000-01-15'), 'M', 'Y'),
        'Empty discount',
      ],
      [
        2.0,
        2,
        new Package(new \DateTime('2000-01-10'), 'L', 'X'),
        'Discount is not limited and equals 2',
      ],
      [
        3.8,
        3.8,
        new Package(new \DateTime('2000-01-20'), 'S', 'Z'),
        'Discount is not limited and equals 4',
      ],
      [
        1.2,
        8.0,
        new Package(new \DateTime('2000-01-15'), 'L', 'X'),
        'Discount is limited to 1',
      ],
      [
        0.0,
        5,
        new Package(new \DateTime('2000-01-15'), 'M', 'Y'),
        'Discount is limited to 0',
      ],
      [
        0.0,
        0.0,
        new Package(new \DateTime('2000-01-15'), 'M', 'Y'),
        'Yet again empty discount 0',
      ],
      [
        -2.2,
        -2.2,
        new Package(new \DateTime('2000-01-15'), 'S', 'W'),
        'Negative discount -2.2',
      ],
      [
        2.2,
        5,
        new Package(new \DateTime('2000-01-15'), 'L', 'N'),
        'Discount is limited to 2.2',
      ],
    ];
  }
}