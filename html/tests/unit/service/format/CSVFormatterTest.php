<?php

namespace tests\unit\service\format;

use PHPUnit\Framework\TestCase;
use src\model\DeliveryPrice;
use src\model\Package;
use src\service\format\CSVFormatter;

class CSVFormatterTest extends TestCase
{
  /** @dataProvider data_provider */
  public function test_format(
    string $expected,
    array $lines,
    array $prices,
    string $description
  ) {
    $formatter = new CSVFormatter();

    $this->assertSame(
      $expected,
      $formatter->format($lines, $prices),
      $description
    );
  }

  public function data_provider()
  {
    $price = $this->getMockBuilder(DeliveryPrice::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['get_price', 'get_discount'])
      ->getMock();

    $price->expects($this->any())
      ->method('get_price')
      ->willReturn(6.9);

    $price->expects($this->atLeastOnce())
      ->method('get_discount')
      ->willReturn(0.33);

    $price2 = $this->getMockBuilder(DeliveryPrice::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['get_price', 'get_discount'])
      ->getMock();

    $price2->expects($this->any())
      ->method('get_price')
      ->willReturn(6.00);

    $price2->expects($this->atLeastOnce())
      ->method('get_discount')
      ->willReturn(0.0);

    return [
      [
        "2015-02-29 CUSPS Ignored\n",
        ['2015-02-29 CUSPS'],
        [null],
        'Replace unrecognised price with keyword',
      ],
      [
        "Test unrecognisable Ignored\n",
        ['Test unrecognisable'],
        [null],
        'Mark unrecognised line with keyword',
      ],
      [
        "Any line 6.90 0.33\n",
        ['Any line'],
        [$price],
        'Append line with price and discount',
      ],
      [
        "Any line 6.00 -\n",
        ['Any line'],
        [$price2],
        'Append line with price and empty discount',
      ],
    ];
  }
}