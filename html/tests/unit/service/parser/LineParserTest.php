<?php

namespace tests\unit\service\parser;

use PHPUnit\Framework\TestCase;
use \src\service\parser\LineParser;

class LineParserTest extends TestCase
{
  /** @dataProvider data_provider */
  public function test_parse(
    ?array $expected,
    ?string $expected_message,
    string $line,
    string $description
  ) {
    $parser = new LineParser();

    if($expected_message){
      $this->expectExceptionMessage($expected_message);
    }

    $package = $parser->parse($line);
    if($expected){
      $this->assertSame(
        $expected['date'],
        $package->get_date()->format('Y-m-d'),
        'Date matches '.$description
      );
      $this->assertSame(
        $expected['size'],
        $package->get_size(),
        'Size matches '.$description
      );
      $this->assertSame(
        $expected['carrier'],
        $package->get_carrier(),
        'Carrier matches '.$description
      );
    }
  }

  public function data_provider()
  {
    return [
      [
        null,
        'Failed to parse package from line "2000-01-01"',
        "2000-01-01",
        'Failed to parse line',
      ],
      [
        null,
        'Failed to parse package from line "2000-01-01 X DPD"',
        "2000-01-01 X DPD",
        'Failed to parse line',
      ],
      [
        null,
        'Failed to parse package from line "2000-01-01 X DPD John"',
        "2000-01-01 X DPD John",
        'Failed to parse line',
      ],
      [
        ['date' => '2020-01-02', 'size' => 'L', 'carrier' => 'LP'],
        null,
        "2020-01-02 L LP",
        'Parsed line',
      ],
    ];
  }
}