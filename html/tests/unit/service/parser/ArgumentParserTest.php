<?php

namespace tests\unit\service\parser;

use PHPUnit\Framework\TestCase;
use src\service\parser\ArgumentParser;

class ArgumentParserTest extends TestCase
{
  /**
   * @dataProvider data_provider
   */
  public function test_get_input_file_path($expected, $args, $description)
  {
    $this->assertSame(
      $expected,
      (new ArgumentParser())->get_input_file_path($args),
      $description
    );
  }

  public function data_provider()
  {
    return [
      [null, [], 'No file provided with empty arguments'],
      [null, ['random param'], 'No file provided with arguments'],
      ['test.txt', ['random', 'test.txt'], '"test.txt" provided with arguments'],
    ];
  }
}