<?php

namespace tests\unit;

use PHPUnit\Framework\TestCase;
use src\App;
use src\service\format\CSVFormatter;
use src\service\parser\ArgumentParser;
use src\service\parser\LineParser;
use src\service\price\PriceCalculator;

class AppTest extends TestCase
{
  public function test_config_exception(): void
  {
    $this->expectErrorMessage('Can not read file "fake-path"');
    $args = [1 => 'fake-path'];
    $app = new App(
      new ArgumentParser(),
      new LineParser(),
      new CSVFormatter(),
      new PriceCalculator()
    );
    $app->run($args);
  }
}