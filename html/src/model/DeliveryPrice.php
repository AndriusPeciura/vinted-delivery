<?php

namespace src\model;

class DeliveryPrice
{
  /** @var Package */
  private $package;
  /** @var float */
  private $price;
  /** @var float */
  private $discount;

  public function __construct(Package $package)
  {
    $this->package = $package;
  }

  public function get_package(): Package
  {
    return $this->package;
  }

  public function get_price(): ?float
  {
    return $this->price;
  }

  public function set_price(float $price): self
  {
    $this->price = $price;
    return $this;
  }

  public function get_discount(): ?float
  {
    return $this->discount;
  }

  public function set_discount(float $discount): self
  {
    $this->discount = $discount;
    return $this;
  }
}