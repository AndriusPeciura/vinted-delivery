<?php

namespace src\model;

use src\exception\InvalidArgumentException;
use \DateTime;

class Package
{
  /** @var \DateTime */
  private $date;
  /** @var string */
  private $size;
  /** @var string */
  private $carrier;

  public function __construct(DateTime $date, string $size, string $carrier)
  {
    $this->date = $date;

    if (!in_array($size, PackageSize::SIZES_ALL)) {
      throw new InvalidArgumentException(
        sprintf('Unknown size "%s"', $size)
      );
    }
    $this->size = $size;
    $this->carrier = $carrier;
  }

  public function get_date(): DateTime
  {
    return $this->date;
  }

  public function get_size(): string
  {
    return $this->size;
  }

  public function get_carrier(): string
  {
    return $this->carrier;
  }
}