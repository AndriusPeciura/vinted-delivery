<?php

namespace src\model\carrier;

use src\exception\EmptyValueException;
use src\exception\PriceNotFoundException;
use src\model\PackageSize;

abstract class CarrierBase implements CarrierInterface
{
  protected const PRICE_LIST = [];
  public const ALIAS = '';

  public function get_alias(): string
  {
    if (!static::ALIAS) {
      throw new EmptyValueException(
        sprintf('Carrier "%s" alias can not be empty.', static::class)
      );
    }

    return static::ALIAS;
  }

  public function get_price(string $size): float
  {
    if (isset(static::PRICE_LIST[$size])) {
      return static::PRICE_LIST[$size];
    }

    $prices = static::PRICE_LIST;
    foreach (PackageSize::SIZES_ALL as $one) {
      if ($size !== $one) {
        continue;
      }
      if (!isset(static::PRICE_LIST[$one])) {
        continue;
      }
      return $prices[$one];
      break;
    }

    throw new PriceNotFoundException(
      sprintf(
        'Carrier "%s" does not have price for size "%s".',
        $this->get_alias(),
        $size
      )
    );
  }
}