<?php

namespace src\model\carrier;

use src\model\PackageSize;

class MRCarrier extends CarrierBase
{
  protected const PRICE_LIST = [
    PackageSize::SIZE_S => 2,
    PackageSize::SIZE_M => 3,
    PackageSize::SIZE_L => 4,
  ];
  public const ALIAS = 'MR';
}