<?php

namespace src\model\carrier;

use src\model\PackageSize;

class LPCarrier extends CarrierBase
{
  protected const PRICE_LIST = [
    PackageSize::SIZE_S => 1.5,
    PackageSize::SIZE_M => 4.9,
    PackageSize::SIZE_L => 6.9,
  ];
  public const ALIAS = 'LP';
}