<?php

namespace src\model\carrier;

use src\exception\InvalidArgumentException;

class CarrierHash
{
  /** @var CarrierInterface[] */
  private $hash = [];

  public function __construct()
  {
    /** @var CarrierInterface[] $carriers */
    $carriers = [new LPCarrier(), new MRCarrier()];
    foreach ($carriers as $carrier) {
      $this->hash[$carrier->get_alias()] = $carrier;
    }
  }

  public function get_hash()
  {
    return $this->hash;
  }

  public function get($alias): CarrierInterface
  {
    if (array_key_exists($alias, $this->hash)) {
      return $this->hash[$alias];
    }
    throw new InvalidArgumentException(
      sprintf(
        'Can not find carrier by alias "%s"',
        $alias
      )
    );
  }
}