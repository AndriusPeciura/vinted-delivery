<?php

namespace src\model\carrier;

interface CarrierInterface
{
  public function get_alias(): string;

  public function get_price(string $size): float;
}