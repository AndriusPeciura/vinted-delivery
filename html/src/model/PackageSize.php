<?php

namespace src\model;

class PackageSize
{
  public const SIZE_S = 'S';
  public const SIZE_M = 'M';
  public const SIZE_L = 'L';

  public const SIZES_ALL = [
    self::SIZE_S,
    self::SIZE_M,
    self::SIZE_L,
  ];
}