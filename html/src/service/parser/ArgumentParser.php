<?php

namespace src\service\parser;

class ArgumentParser
{
  public function get_input_file_path($args){
    return $args[1] ?? null;
  }
}