<?php

namespace src\service\parser;

use src\exception\InvalidArgumentException;
use src\model\Package;
use \DateTime;

class LineParser
{
  private const LINE_PATTERN
    = '/^(?<date>\d{4}-\d{2}-\d{2}) (?<size>[S|M|L]) (?<carrier>[A-Z]{2})$/';

  /**
   * @throws InvalidArgumentException
   */
  public function parse(string $line): ?Package
  {
    $matches = [];
    if(preg_match(self::LINE_PATTERN, $line, $matches)){
      return new Package(
        new DateTime($matches['date']),
        $matches['size'],
        $matches['carrier']
      );
    }

    throw new InvalidArgumentException(
      sprintf('Failed to parse package from line "%s"', $line)
    );
  }
}