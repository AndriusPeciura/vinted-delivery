<?php

namespace src\service\price;

use src\model\carrier\CarrierHash;
use src\model\DeliveryPrice;
use src\model\Package;

class CarrierPriceCalculator
{
  /** @var CarrierHash */
  private $carrier_hash;

  public function __construct()
  {
    $this->carrier_hash = new CarrierHash();
  }

  public function calculate(Package $package): DeliveryPrice
  {
    $price = $this->carrier_hash
      ->get($package->get_carrier())
      ->get_price($package->get_size());
    $deliveryPrice = new DeliveryPrice($package);
    $deliveryPrice->set_price($price);
    return $deliveryPrice;
  }
}