<?php

namespace src\service\price\discount;

use src\model\Package;

interface DiscountCalculatorInterface
{
  public function calculate(float $price, Package $package): float;
}