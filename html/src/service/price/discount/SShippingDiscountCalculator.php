<?php

namespace src\service\price\discount;

use src\model\carrier\CarrierHash;
use src\model\Package;
use src\model\PackageSize;

class SShippingDiscountCalculator implements DiscountCalculatorInterface
{
  /** @var CarrierHash */
  private $carrier_hash;

  public function __construct()
  {
    $this->carrier_hash = new CarrierHash();
  }

  public function calculate(float $price, Package $package): float
  {
    if ($package->get_size() !== PackageSize::SIZE_S) {
      return 0.0;
    }
    $min_price = $price;
    foreach ($this->carrier_hash->get_hash() as $carrier) {
      $carrier_price = $carrier->get_price($package->get_size());
      if ($min_price > $carrier_price) {
        $min_price = $carrier_price;
      }
    }

    return $price - $min_price;
  }
}