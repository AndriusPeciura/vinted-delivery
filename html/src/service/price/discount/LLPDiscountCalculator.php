<?php

namespace src\service\price\discount;

use src\model\carrier\LPCarrier;
use src\model\Package;
use src\model\PackageSize;

class LLPDiscountCalculator implements DiscountCalculatorInterface
{
  /** @var array [$month => [$package, ...], ...] */
  private $package_hash;

  public function calculate(float $price, Package $package): float
  {
    if ($package->get_carrier() === LPCarrier::ALIAS
      && $package->get_size() === PackageSize::SIZE_L) {
      $month = $package->get_date()->format('Y-m');
      $this->package_hash[$month][] = $package;

      if (count($this->package_hash[$month]) === 3) {
        return $price;
      }
    }

    return 0.0;
  }
}