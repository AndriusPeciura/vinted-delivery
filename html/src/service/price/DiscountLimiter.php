<?php

namespace src\service\price;

use src\model\Package;

class DiscountLimiter
{
  private const MAX = 10;

  /** @var array [$month => $discount] */
  private $discountHash = [];

  public function limit(float $discount, Package $package)
  {
    $month = $package->get_date()->format('Y-m');
    if (!isset($this->discountHash[$month])) {
      $this->discountHash[$month] = 0.0;
    }

    $left = self::MAX - $this->discountHash[$month];

    if ($left < $discount) {
      $discount = $left;
    }
    $this->discountHash[$month] += $discount;

    return $discount;
  }
}