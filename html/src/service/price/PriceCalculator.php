<?php

namespace src\service\price;

use src\model\DeliveryPrice;
use src\model\Package;
use src\service\price\discount\DiscountCalculatorInterface;
use src\service\price\discount\LLPDiscountCalculator;
use src\service\price\discount\SShippingDiscountCalculator;

class PriceCalculator
{
  /** @var CarrierPriceCalculator */
  private $carrier_price_calculator;
  /** @var DiscountCalculatorInterface[] */
  private $discount_calculators = [];
  /** @var DiscountLimiter */
  private $discount_limiter;

  public function __construct()
  {
    $this->carrier_price_calculator = new CarrierPriceCalculator();
    $this->discount_calculators = [
      new LLPDiscountCalculator(),
      new SShippingDiscountCalculator(),
    ];
    $this->discount_limiter = new DiscountLimiter();
  }

  public function calculate(Package $package): DeliveryPrice
  {
    $discount = 0.0;
    $delivery_price = $this->carrier_price_calculator->calculate($package);
    foreach ($this->discount_calculators as $calculator) {
      $discount += $calculator->calculate($delivery_price->get_price(), $package);
    }
    $discount = $this->discount_limiter->limit($discount, $package);
    $delivery_price->set_price($delivery_price->get_price() - $discount);
    $delivery_price->set_discount($discount);

    return $delivery_price;
  }
}