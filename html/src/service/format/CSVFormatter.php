<?php

namespace src\service\format;

use src\model\DeliveryPrice;

class CSVFormatter
{
  public function format(array $lines, array $prices): string
  {
    return implode(
        "\n",
        array_map(
          [$this, 'format_line'],
          $lines,
          $prices
        )
      ) . "\n";
  }

  private function format_line(string $line, ?DeliveryPrice $price)
  {
    if ($price) {
      return sprintf(
        '%s %s %s',
        $line,
        number_format($price->get_price(), 2),
        $price->get_discount()
          ? number_format($price->get_discount(), 2)
          : '-'
      );
    }

    return sprintf(
      '%s %s',
      $line,
      'Ignored'
    );
  }
}