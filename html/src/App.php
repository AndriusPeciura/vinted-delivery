<?php

namespace src;

use src\model\Config;
use src\model\Package;
use src\service\format\CSVFormatter;
use src\service\parser\ArgumentParser;
use src\exception\InvalidArgumentException;
use src\service\parser\LineParser;
use src\service\price\PriceCalculator;

class App
{
  /** @var ArgumentParser */
  private $argument_parser;
  /** @var LineParser */
  private $line_parser;
  /** @var CSVFormatter */
  private $formatter;
  /** @var PriceCalculator */
  private $price_calculator;

  public function __construct(
    ArgumentParser $argument_parser,
    LineParser $line_parser,
    CSVFormatter $formatter,
    PriceCalculator $price_calculator
) {
    $this->argument_parser = $argument_parser;
    $this->line_parser = $line_parser;
    $this->formatter = $formatter;
    $this->price_calculator = $price_calculator;
  }

  public function run(array $args)
  {
    $config = $this->build_config($args);

    $this->check_file_is_readable($config->input_path);
    $prices = [];
    $lines = $this->read_lines($config->input_path);
    foreach ($lines as $i => $line) {
      $package = $this->parse_line($line);
      $prices[$i] = $package
        ? $this->price_calculator->calculate($package)
        : null;
    }

    return $this->formatter->format($lines, $prices);
  }

  protected function build_config(array $args)
  {
    $config = new Config();

    $file_input_path = $this->argument_parser->get_input_file_path($args);
    if ($file_input_path !== null) {
      $config->input_path = $file_input_path;
    }

    return $config;
  }

  protected function check_file_is_readable(string $path)
  {
    if (!is_readable($path)) {
      throw new \InvalidArgumentException(
        sprintf(
          'Can not read file "%s"',
          $path
        )
      );
    }
    return $this;
  }

  protected function read_lines(string $file_path): array
  {
    $content = file_get_contents($file_path);
    return explode("\n", trim($content));
  }

  protected function parse_line(string $line): ?Package
  {
    try {
      return $this->line_parser->parse($line);
    } catch (InvalidArgumentException $exception) {
      return null;
    }
  }
}