<?php

use src\service\format\CSVFormatter;
use src\service\parser\ArgumentParser;
use src\service\parser\LineParser;
use src\service\price\PriceCalculator;

require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'autoload.php';

exit(
(new src\App(
  new ArgumentParser(),
  new LineParser(),
  new CSVFormatter(),
  new PriceCalculator()
))->run($argv)
);