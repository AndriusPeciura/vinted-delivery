FROM php:7.3-apache-stretch

RUN apt-get update && apt-get install -y libzip-dev libz-dev

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

ADD config/php.ini /usr/local/etc/php/conf.d/custom.ini
ADD config/000-default.conf /etc/apache2/sites-available/000-default.conf

RUN echo "y" | pecl install apcu
RUN docker-php-ext-install zip pdo_mysql
RUN docker-php-ext-enable opcache apcu

RUN a2enmod rewrite

RUN curl -Lo  /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-9.phar
RUN chmod +x /usr/local/bin/phpunit

WORKDIR /var/www/html
COPY html /var/www/html
CMD mkdir -p var/log && chmod -R 777 var/log && \
    mkdir -p var/storage && chmod -R 777 var/storage && \
    apache2-foreground
